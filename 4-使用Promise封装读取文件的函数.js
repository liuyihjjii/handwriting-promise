

// 使用Promise封装取文件的函数
 function minreadFile(path) {
   return new Promise((resolve,reject) => {
      // 引入读取文件的魔偶快
     require('fs').readFile(path,(err,data) => {
       // 如果出现错误 抛出错误
       if(err) reject(err)
       resolve(data)
     })
   })
 }


 // 调用读取文件的函数

 // 由于该函数的返回值时一个promise 的对象，所以可以直接通过 .then的方式获取成功或者失败的回调
 minreadFile('./resour/content.txt')
 .then(value => {
   console.log(value.toString())
 },reson => {
   console.log(reson)
 })