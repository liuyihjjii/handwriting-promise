// 引入 util 模块
const util = require('util')

// 引入fs模块

const fs = require('fs')


// 返回一个新的函数

// 使用util下的 promisity方法 将之前fs模块下的回调形式的读取文件的形式改为promise 形式
let minreadFile = util.promisify(fs.readFile)

// util.promisity返回的饿是一个promise封装的模块
minreadFile('./resour/content.txt').then(value => {
  console.log(value.toString())
})
