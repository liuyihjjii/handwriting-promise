// 首先声明构造函数
function Promise(executor) {

  // 添加属性
  this.promiseState = 'pending';
  this.promiseResult = 'null'


  // 添加callback回调
  // 指定多个回调的时候将回调函数村存放在一个数组中
  this.callbacks = [];


  // 保存初始的this

  const that = this
  // resolve
  function resolve(data) {
    // 当promise 的状态发生改变的时候就无法修改
    if (that.promiseState !== 'pending') return
    // 当调用resolve时修改promise的状态
    that.promiseState = 'fullfilled'
    that.promiseResult = data

    // 如果有异步任务 并且执行的是成功的回调，就调用这个方法
    // that.callback.onResolve(data)
    // 成功时候的回调

    // 指定多个回调的时候将回调的数组进行遍历，调用成功或者失败的结果

    that.callbacks.forEach(item => {
      item.onResolve(data)
    })
  }
  // reject
  function reject(data) {
    // 当promise 的状态发生改变的时候就无法修改
    if (that.promiseState !== 'pending') return
    // 当调用resolve时修改promise的状态
    that.promiseState = 'rejected'
    that.promiseResult = data
    // 如果有异步任务 并且执行的是失败的回调，就调用这个方法
    // that.callback.onReject(data)
    //当指定多个回调的时候
    that.callbacks.forEach(item => {
      item.onReject(data)
    })
  }

  // 抛出异常
  try {
    // 同步调用执行器函数
    executor(resolve, reject)
  } catch (error) {
    reject(error)
  }

}



// 给构造函数添加then方法
Promise.prototype.then = function (onResolve, onReject) {

  // 保存 this
  const that = this

  return new Promise((resolve, reject) => {

    function callback(type) {
      // 调用 。then 那肯定就是成功的结果
      try {

        let result = type(that.promiseResult)
        // 对成功的结果进行判断
        if (result instanceof Promise) {
          // 如果返回值是一个Promise的话 就对返回值掉then方法
          result.then(v => {
            resolve(v)
          }, r => {
            reject(r)
          })
        } else {
          resolve(result)
        }

      } catch (error) {
        // 有异常抛出错误 catch捕捉 调用reject就可以
        reject(error)
      }
    }

    // 当 .then 时判断promiseSate的状态
    if (this.promiseState === 'fullfilled') {
      callback(onResolve)

    }

    // 执行失败的时候

    if (this.promiseState === 'rejected') {
      callback(onReject)
    }

    // 判断promise 的状态

    if (this.promiseState === 'pending') {

      // 将指定的多个回调存放在callbacks这个数组中
      this.callbacks.push({
        // 异步调用 then 方法
        onResolve: function () {
          // 处理抛出的一场结果
          callback(onResolve)
        },
        onReject: function () {
          //  处理抛出的异常
          callback(onReject)
        }
      })
    }

  })
}




// 给构造函数添加catch方法
Promise.prototype.catch = function(onReject) {
  return this.then(undefined,onReject)
}