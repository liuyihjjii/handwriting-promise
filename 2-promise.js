// 首先引进fs模块
const fs = require('fs')

// 读取文件
// err为读取文件出错的时候 抛出的错误
// data为读取文件时文件的内容

// 函数回调的形式

// fs.readFile('./resour/content.txt',(err,data) => {
  
//     // 出错 抛出错误
//   if(err) throw err

//   // 输出文件内容
//   console.log(data.toString())
// })


let p = new Promise((resolve,reject) => {
  fs.readFile('./resour/conten.txt',(err,data) => {
    if(err) reject(err)
    resolve(data)
  })
})

p.then(value => {
  console.log(value.toString())
}).catch(reson=> {
  console.log(reson)
})